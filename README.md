# Vault
Vault - Hashicorp Vault - Docker compose

## Getting Start
Ejecutar 
```bash
docker-compose up -d
```
Esto levanta el container con el vault de Hashicorp, el mismo se puede acceder desde ``localhost:8200``.
Una vez levantado el vault se puede correr el código de ``index.ts`` ejecutando 
```
npm start  
```

## TODO
Listado de cosas que nos quedaron por investigar.
- Manejo de accesos (r/w roles, etc.).
- Production config.

### Links
- https://developer.hashicorp.com/vault/tutorials/getting-started/getting-started-deploy
- https://www.google.com/search?q=running+hashicorp+vault+for+production&sourceid=chrome&ie=UTF-8
- https://hub.docker.com/r/hashicorp/vault
- https://github.com/shahradelahi/node-vault/tree/master
- https://developer.hashicorp.com/vault/api-docs/libraries
- https://www.youtube.com/watch?v=gmk01iDn36c
- https://github.com/hashicorp/vault/tree/main
- https://gist.github.com/Mishco/b47b341f852c5934cf736870f0b5da81
