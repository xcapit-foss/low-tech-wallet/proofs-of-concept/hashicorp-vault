import { Client } from "@litehex/node-vault";


const mountPath = "secret";
const path = "hello";

const vc = new Client({
  apiVersion: "v1",
  endpoint: "http://127.0.0.1:8200",
  token: "root",
});

const writeAKey = async () => {
  const write = await vc.kv2.write({
    mountPath,
    path,
    data: { foo: "bar" },
  });
  console.log(write);
};

// writeAKey();

const read = async () => await vc.kv2.read({ mountPath, path });
(async function() {
  console.log(await read());
})(); 

